var tabla=[


];

window.onload=cargarEventos;

function cargarEventos(){

document.getElementById("mostrar-evento").addEventListener("click",mostrarTabla,false);
document.getElementById("nuevo-evento").addEventListener("submit",nuevoEvento,false);

}

function mostrarTabla(){
    var cuerpoTabla=document.getElementById("evento-tabla");
    var tablallena="";
    for(let i=0;i<tabla.length;i++){
        tablallena+="<tr><td>"+tabla[i].evento+"</td><td>"+tabla[i].fecha+"</td></tr>";
        
        
    }
   
    cuerpoTabla.innerHTML=tablallena;
    
}
function nuevoEvento(event){
    event.preventDefault();
    var eventoIntroducidoPorUsuario=document.getElementById("evento").value
    var fechaIntroducidoPorUsuario=document.getElementById("fecha").value
    var nuevoEvento={evento :eventoIntroducidoPorUsuario, fecha:fechaIntroducidoPorUsuario};
    tabla.push(nuevoEvento);
    alert("El evento ha sido agregado con éxito");
  ordenartabla();
}
   
function ordenartabla(){
 
    tabla.sort(function (a, b) {
        if (a.fecha > b.fecha) {
          return 1;
        }
        if (a.fecha < b.fecha) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
guardarlocal();

}
function guardarlocal(){

localStorage.setItem("elementos" , JSON.stringify(  tabla ));
}
function mostrarLocal(){

var local=  JSON.parse(localStorage.getItem( "elementos" ));
tabla=local;
mostrarTabla();
}
